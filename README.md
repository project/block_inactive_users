CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The Block Inactive Users module automatically blocks user who haven't been
active for a designated amount of time and cancel users based on configurations.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/block_inactive_users

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/block_inactive_users


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to "Adminstration > Extend" and enable module.
    2. Navigate to "Adminstration > Configuration > People > Block inactive
       users" to configure options for automatically blocking inactive users.
        1. Enter the time period, in months, of inactivity to disable users.
        2. Configure an email for the inactive user. Fill in the admin's email
           address and the subject fields. Use of tokens is available in the
            help text.
        3. Pressing the 'Disable inactive users' button will immediately
           invoke the script.
        4. Save configuration to run on the next Drupal cron jobs.
    3. Navigate to "Adminstration > Configuration > People > Cancel users"
           to configure options for cancel users.
        1.  Enter the time period, in months, of inactivity to disable users.
        2.  Check Include users who have never logged in.
        3.  Select Include users with role(s).
        4.  Include users with status.
        5.  Whitelist user(s) by username.
        6.  Whitelist user(s) by mail.
        7.  Select cancelling account(s) type.
        8.  Require email confirmation to cancel account.
        9.  Pressing the 'Cancel Users' button will immediately
            invoke the script.
        10. Save configuration to see what selections was done.
