<?php

namespace Drupal\block_inactive_users\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * The ReactivateUserController Class.
 *
 * @package Drupal\block_inactive_users\Controller
 */
class ReactivateUserController extends ControllerBase {

  use MessengerTrait;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * Constructs a UserController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The user storage.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger instance.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A time service.
   */
  public function __construct(DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager, UserDataInterface $user_data, LoggerChannelFactoryInterface $logger_factory, TimeInterface $time) {
    $this->dateFormatter = $date_formatter;
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->userData = $user_data;
    $this->logger = $logger_factory->get('user');
    $this->timeService = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('user.data'),
      $container->get('logger.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * Confirms reactivating a user account via an email link.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   * @param int $timestamp
   *   The timestamp.
   * @param string $hashed_pass
   *   The hashed password.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function confirmReactivate(UserInterface $user, $timestamp = 0, $hashed_pass = '') {

    $current = $this->timeService->getRequestTime();

    // Basic validation of arguments.
    if (!empty($timestamp) && !empty($hashed_pass)) {
      // Validate expiration and hashed password/login.
      if ($timestamp <= $current && $user->id() && $timestamp >= $user->getLastLoginTime() && hash_equals($hashed_pass, user_pass_rehash($user, $timestamp))) {
        $user->activate();
        // In order to expire this reactivate link,
        // the user login time has to be updated.
        $user->setLastLoginTime($current);
        $user->save();
        $this->messenger()->addMessage($this->t('Your account has been activated'));
        return $this->redirect('<front>');
      }
      else {
        $this->messenger()->addError($this->t('You have tried to use an account reactivation link that is invalid.'));
        return $this->redirect('<front>');
      }
    }
    throw new AccessDeniedHttpException();
  }

}
