<?php

namespace Drupal\block_inactive_users\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class confirmCancelUsersForm.
 *
 * @package Drupal\block_inactive_users\Form
 */
class ConfirmCancelUsersForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['block_inactive_users.confirm_cancel_users_form'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_cancel_users_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $count = block_inactive_users_block_cancel_users("count");
    return $this->t('Are you sure you want to cancel %count users based on the rules set?', ['%count' => $count]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('block_inactive_users.settings_cancel_users');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Confirm');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Cancel Users based on configured rules.
    block_inactive_users_block_cancel_users("cancel");

    $this->messenger()->addMessage($this->t('Below users are now canceled based on the rules set:'));
    $form_state->setRedirectUrl($this->getCancelUrl());

  }

}
