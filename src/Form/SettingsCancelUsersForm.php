<?php

namespace Drupal\block_inactive_users\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The SettingsCancelUsersForm Class.
 *
 * @package Drupal\block_inactive_users\Form
 */
class SettingsCancelUsersForm extends ConfigFormBase {

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * An entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Create function for dependency injection.
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerChannelFactory = $loggerChannelFactory;

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "block_inactive_users_settings";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['block_inactive_users.settings_cancel_users'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('block_inactive_users.settings_cancel_users');

    $role_entities = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $roles = [];
    foreach ($role_entities as $role) {
      $roles[$role->id()] = $role->label();
    }

    $form["users_settings"] = [
      "#type" => "details",
      "#title" => $this->t("Cancel Users settings"),
      '#open' => TRUE,
    ];

    $form['users_settings']['block_inactive_users_idle_time'] = [
      '#title' => $this->t('Time period (in months)'),
      '#required' => TRUE,
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#default_value' => $config->get('block_inactive_users_idle_time'),
      '#description' => $this->t('Cancel inactive users.'),
    ];

    $form['users_settings']['block_inactive_users_include_never_accessed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include users who have never logged in'),
      '#default_value' => $config->get('block_inactive_users_include_never_accessed'),
      '#description' => $this->t('If checked, will block users whose last access is set to "never."'),
    ];

    $form['users_settings']['block_inactive_users_include_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Include users with role(s):'),
      '#default_value' => $config->get('block_inactive_users_include_roles'),
      '#options' => $roles,
      '#size' => count($roles),
      '#description' => $this->t('Select role(s) to disabling account.'),
    ];

    $form['users_settings']['block_inactive_users_include_status'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Include users with status:'),
      '#default_value' => $config->get('block_inactive_users_include_status'),
      '#options' => [
        '0' => $this->t('Blocked'),
        '1' => $this->t('Active'),
      ],
      '#size' => 2,
      '#description' => $this->t('Select status to disabling account.'),
    ];

    $form['users_settings']['block_inactive_users_whitelist_user'] = [
      '#title' => $this->t('Whitelist user(s) by username'),
      '#type' => 'textarea',
      '#attributes' => ['placeholder' => $this->t('abc123')],
      '#default_value' => $config->get('block_inactive_users_whitelist_user'),
      '#description' => $this->t('Whitelist user(s) by username separated per line.'),
    ];

    $form['users_settings']['block_inactive_users_whitelist_email'] = [
      '#title' => $this->t('Whitelist user(s) by mail'),
      '#type' => 'textarea',
      '#attributes' => ['placeholder' => $this->t('@test.com')],
      '#default_value' => $config->get('block_inactive_users_whitelist_email'),
      '#description' => $this->t('Whitelist user(s) by mail separated per line.'),
    ];

    $form['users_settings']['block_inactive_users_disable_account_method'] = [
      '#type' => 'select',
      '#title' => $this->t('When cancelling the account(s):'),
      '#default_value' => $config->get('block_inactive_users_disable_account_method'),
      '#options' => [
        'user_cancel_block' => $this->t('Disable the account and keep its content.') . ' - ' . $this->t('Your account will be blocked and you will no longer be able to log in. All of your content will remain attributed to your username.'),
        'user_cancel_block_unpublish' => $this->t('Disable the account and unpublish its content.') . ' - ' . $this->t('Your account will be blocked and you will no longer be able to log in. All of your content will be hidden from everyone but administrators.'),
        'user_cancel_reassign' => $this->t("Delete the account and make its content belong to the Anonymous user. Reassign its groups to the super administrator."),
        'user_cancel_delete' => $this->t('Delete the account and its content.') . ' - ' . $this->t('Your account will be removed and all account information deleted. All of your content will also be deleted.'),
      ],
      '#description' => $this->t('Select the method to cancel the accounts. This action cannot be undone.'),
    ];

    $form['users_settings']['block_inactive_users_cancel_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require email confirmation to cancel account'),
      '#default_value' => $config->get('block_inactive_users_cancel_email'),
      '#description' => $this->t('When enabled, the user must confirm the account cancellation via email.'),
      '#attributes' => [
        'name' => 'block_inactive_users_cancel_email',
      ],
    ];

    $form['actions']['block_inactive_users_cancel_users_update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel Users'),
      '#submit' => ['::updateCancelUsers'],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('block_inactive_users.settings_cancel_users')
      ->set('block_inactive_users_idle_time', $form_state->getValue('block_inactive_users_idle_time'))
      ->set('block_inactive_users_include_never_accessed', $form_state->getValue('block_inactive_users_include_never_accessed'))
      ->set('block_inactive_users_include_roles', $form_state->getValue('block_inactive_users_include_roles'))
      ->set('block_inactive_users_include_status', $form_state->getValue('block_inactive_users_include_status'))
      ->set('block_inactive_users_whitelist_user', $form_state->getValue('block_inactive_users_whitelist_user'))
      ->set('block_inactive_users_whitelist_email', $form_state->getValue('block_inactive_users_whitelist_email'))
      ->set('block_inactive_users_disable_account_method', $form_state->getValue('block_inactive_users_disable_account_method'))
      ->set('block_inactive_users_cancel_email', $form_state->getValue('block_inactive_users_cancel_email'))
      ->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * Cancel listed users.
   *
   * Remove users after they have idle for time period.
   */
  public function updateCancelUsers(array &$form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);

    $url = Url::fromRoute('block_inactive_users.confirm_cancel_users_form');
    $form_state->setRedirectUrl($url);
  }

}
