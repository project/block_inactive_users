<?php

namespace Drupal\block_inactive_users\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The SettingsForm Class.
 *
 * @package Drupal\block_inactive_users\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * An entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Create function for dependency injection.
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerChannelFactory = $loggerChannelFactory;

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "block_inactive_users_settings";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['block_inactive_users.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('block_inactive_users.settings');
    $role_entities = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $roles = [];
    foreach ($role_entities as $role) {
      if ($role->id() === 'anonymous') {
        continue;
      }
      $roles[$role->id()] = $role->label();
    }

    $form["users_settings"] = [
      "#type" => "details",
      "#title" => $this->t("Inactive Users Management settings"),
      '#open' => TRUE,
    ];
    $form["email_settings"] = [
      "#type" => "details",
      "#title" => $this->t("Email blocked user"),
      '#open' => TRUE,
    ];
    $form["warn_settings"] = [
      "#type" => "details",
      "#title" => $this->t("Email pending blocked user"),
      '#open' => TRUE,
    ];

    $form['users_settings']['block_inactive_users_idle_time'] = [
      '#title' => $this->t('Time period (in months)'),
      '#required' => TRUE,
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#default_value' => $config->get('block_inactive_users_idle_time'),
      '#description' => $this->t('Disable inactive users.'),
    ];

    $form['users_settings']['block_inactive_users_exclude_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Exclude users with role(s):'),
      '#default_value' => $config->get('block_inactive_users_exclude_roles'),
      '#options' => $roles,
      '#description' => $this->t('Select role(s) to avoid disabling account.'),
    ];

    $form['users_settings']['block_inactive_users_include_never_accessed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include users who have never logged in'),
      '#default_value' => $config->get('block_inactive_users_include_never_accessed'),
      '#description' => $this->t('If checked, will block users whose last access is set to "never."'),
    ];

    $form['email_settings']['block_inactive_users_send_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send email'),
      '#default_value' => $config->get('block_inactive_users_send_email'),
      '#description' => $this->t('If checked, will send email to blocked user. When unchecked, will "silently" block.'),
      '#attributes' => [
        'name' => 'block_inactive_users_send_email',
      ],
    ];

    $form['email_settings']['block_inactive_users_from_email'] = [
      '#title' => $this->t('From'),
      '#type' => 'textfield',
      '#default_value' => $config->get('block_inactive_users_from_email'),
      '#description' => $this->t('Email "from" email address configuration.'),
      '#states' => [
        // Only required if the selected to send emails.
        'required' => [
          ':input[name="block_inactive_users_send_email"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['email_settings']['block_inactive_users_email_subject'] = [
      '#title' => $this->t('Email subject'),
      '#type' => 'textfield',
      '#default_value' => $config->get('block_inactive_users_email_subject'),
      '#description' => $this->t('Email subject text.'),
    ];

    $form['email_settings']['block_inactive_users_email_content'] = [
      '#title' => $this->t('Email template'),
      '#type' => 'textarea',
      '#default_value' => $config->get('block_inactive_users_email_content'),
      '#description' => $this->t('Provide a template of email to be sent to disabled users. <br/>Additional replacement tokens available: [days-until-blocked], [activation-link].'),
    ];

    $form['warn_settings']['block_inactive_users_warn_send_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send email'),
      '#default_value' => $config->get('block_inactive_users_warn_send_email'),
      '#description' => $this->t('If checked, will send email to pending blocked user. When unchecked, will do nothing.'),
      '#attributes' => [
        'name' => 'block_inactive_users_warn_send_email',
      ],
    ];

    $form['warn_settings']['block_inactive_users_days_until_blocked'] = [
      '#title' => $this->t('Time period (in days)'),
      '#required' => TRUE,
      '#type' => 'number',
      '#attributes' => [
        'min' => 0,
      ],
      '#default_value' => $config->get('block_inactive_users_days_until_blocked'),
      '#description' => $this->t('Warn inactive users this number of days prior to block.'),
    ];

    $form['warn_settings']['block_inactive_users_warn_from_email'] = [
      '#title' => $this->t('From'),
      '#type' => 'textfield',
      '#default_value' => $config->get('block_inactive_users_warn_from_email'),
      '#description' => $this->t('Email "from" email address configuration.'),
      '#states' => [
        // Only required if the selected to send emails.
        'required' => [
          ':input[name="block_inactive_users_warn_send_email"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['warn_settings']['block_inactive_users_warn_email_subject'] = [
      '#title' => $this->t('Email subject'),
      '#type' => 'textfield',
      '#default_value' => $config->get('block_inactive_users_warn_email_subject'),
      '#description' => $this->t('Email subject text.'),
    ];

    $form['warn_settings']['block_inactive_users_warn_email_content'] = [
      '#title' => $this->t('Email template'),
      '#type' => 'textarea',
      '#default_value' => $config->get('block_inactive_users_warn_email_content'),
      '#description' => $this->t('Provide a template of email to be sent to disabled users. <br/>Additional replacement tokens available: [days-until-blocked], [activation-link].'),
    ];

    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user'],
    ];

    $form['actions']['block_inactive_users_update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disable inactive users'),
      '#submit' => ['::updateUsers'],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('block_inactive_users.settings')
      ->set('block_inactive_users_idle_time', $form_state->getValue('block_inactive_users_idle_time'))
      ->set('block_inactive_users_exclude_roles', $form_state->getValue('block_inactive_users_exclude_roles'))
      ->set('block_inactive_users_include_never_accessed', $form_state->getValue('block_inactive_users_include_never_accessed'))
      ->set('block_inactive_users_send_email', $form_state->getValue('block_inactive_users_send_email'))
      ->set('block_inactive_users_from_email', $form_state->getValue('block_inactive_users_from_email'))
      ->set('block_inactive_users_email_subject', $form_state->getValue('block_inactive_users_email_subject'))
      ->set('block_inactive_users_email_content', $form_state->getValue('block_inactive_users_email_content'))
      ->set('block_inactive_users_warn_send_email', $form_state->getValue('block_inactive_users_warn_send_email'))
      ->set('block_inactive_users_days_until_blocked', (int) $form_state->getValue('block_inactive_users_days_until_blocked'))
      ->set('block_inactive_users_warn_from_email', $form_state->getValue('block_inactive_users_warn_from_email'))
      ->set('block_inactive_users_warn_email_subject', $form_state->getValue('block_inactive_users_warn_email_subject'))
      ->set('block_inactive_users_warn_email_content', $form_state->getValue('block_inactive_users_warn_email_content'))
      ->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * Update users status.
   *
   * Disable users after they have idle for time period.
   */
  public function updateUsers(array &$form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);
    // Disable inactive users based on configured rules.
    block_inactive_users_block_users();
  }

}
