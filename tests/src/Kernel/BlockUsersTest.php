<?php

namespace Drupal\Tests\block_inactive_users\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\User;

/**
 * Checks that automatic block of old users works as expected.
 *
 * @group block_inactive_users
 */
class BlockUsersTest extends KernelTestBase {

  use UserCreationTrait;
  use AssertMailTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block_inactive_users', 'system', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installConfig(['block_inactive_users']);
  }

  /**
   * Users that haven't logged in in 3 months should be blocked.
   */
  public function testInactiveUsers() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this
      ->container
      ->get('config.factory')
      ->getEditable('block_inactive_users.settings');
    $config->set('block_inactive_users_include_never_accessed', TRUE);
    // We avoid a join in a table that does not exists.
    $config->set('block_inactive_users_exclude_roles', []);
    $config->save();

    $new_user = $this->createUser();
    $active = $this->createUser();
    $never_login = $this->createUser();
    $inactive = $this->createUser();

    $this->assertFalse($new_user->isBlocked());
    $this->assertFalse($active->isBlocked());
    $this->assertFalse($never_login->isBlocked());
    $this->assertFalse($inactive->isBlocked());

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = \Drupal::service('database');

    $connection->update('users_field_data')
      ->fields([
        'created' => strtotime('-1 year'),
        'login' => strtotime('-5 days'),
        'access' => strtotime('-5 days'),
      ])
      ->condition('uid', $active->id())
      ->execute();

    $connection->update('users_field_data')
      ->fields([
        'created' => strtotime('-1 year'),
        'login' => strtotime('-120 days'),
        'access' => strtotime('-120 days'),
      ])
      ->condition('uid', $inactive->id())
      ->execute();

    $connection->update('users_field_data')
      ->fields([
        'created' => strtotime('-1 year'),
        'login' => NULL,
      ])
      ->condition('uid', $never_login->id())
      ->execute();

    block_inactive_users_block_users();

    $new_user = User::load($new_user->id());
    $active = User::load($active->id());
    $inactive = User::load($inactive->id());
    $never_login = User::load($never_login->id());

    $this->assertFalse($new_user->isBlocked());
    $this->assertFalse($active->isBlocked());
    $this->assertTrue($inactive->isBlocked());
    $this->assertTrue($never_login->isBlocked());

    // Verify that re-saving a blocked user does not update the
    // last access time.
    $original_last_access_time = $inactive->getLastAccessedTime();
    $inactive->save();
    $inactive = User::load($inactive->id());
    $this->assertEquals($original_last_access_time, $inactive->getLastAccessedTime());

    // Verify that re-activating a blocked user updates last access time and the
    // user is not re-blocked when we re-run the block users function.
    $inactive->activate();
    $inactive->save();
    block_inactive_users_block_users();
    $inactive = User::load($inactive->id());
    $this->assertFalse($inactive->isBlocked());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $inactive->getLastAccessedTime());
  }

  /**
   * Test warning email.
   */
  public function testWarnUsers() {
    $config = $this
      ->container
      ->get('config.factory')
      ->getEditable('block_inactive_users.settings');

    $config
      ->set('block_inactive_users_include_never_accessed', TRUE)
      ->set('block_inactive_users_exclude_roles', [])
      ->save();

    // Create unused user 1 since the module excludes user 1 from ban.
    $this->createUser();
    $warn_user = $this->createUser();

    // Create login time that falls within warning window
    // (5 days shy of 3 months).
    $last_login = strtotime('+5 days', strtotime('-3 months'));

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = \Drupal::service('database');

    $connection->update('users_field_data')
      ->fields([
        'created' => strtotime('-1 year'),
        'login' => $last_login,
        'access' => $last_login,
      ])
      ->condition('uid', $warn_user->id())
      ->execute();

    // Warn user.
    $this
      ->container
      ->get('block_inactive_users.deactivate_users')
      ->warn();

    // Verifiy correct email recieved.
    $site_name = $this
      ->container
      ->get('config.factory')
      ->get('system.site')
      ->get('name');

    // Confirm email send.
    $this
      ->assertMailString('subject', "Pending account block on {$site_name}", 1);
  }

}
