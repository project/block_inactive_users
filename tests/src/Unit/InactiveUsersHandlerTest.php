<?php

namespace Drupal\Tests\block_inactive_users\Unit;

use Drupal\block_inactive_users\InactiveUsersHandler;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group block_inactive_users
 */
class InactiveUsersHandlerTest extends UnitTestCase {
  const FORM_SETTINGS_CONFIG_OBJ_NAME = "block_inactive_users.settings";
  const LOGGER_CHANNEL = "block_inactive_users";

  /**
   * A config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactoryService;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * An language manager instance.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * A time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * A state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * A token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * An entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $enEntityTypeManager;

  /**
   * A mail manager instance.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Before a test method is run, setUp() is invoked.
   */
  public function setUp(): void {

    parent::setUp();

    $container = new ContainerBuilder();

    $this->configFactoryService = $this->getConfigFactoryStub([]);
    $this->logger = $this->getLoggerFactoryMock();
    $this->languageManager = $this->createMock('\\Drupal\\Core\\Language\\LanguageManagerInterface');
    $this->timeService = $this->createMock('\\Drupal\\Component\\Datetime\\TimeInterface');
    $this->enEntityTypeManager = $this->createMock('\\Drupal\\Core\\Entity\\EntityTypeManagerInterface');
    $this->mailManager = $this->createMock('\\Drupal\\Core\\Mail\\MailManagerInterface');
    $this->state = $this->createMock('\\Drupal\\Core\\State\\StateInterface');
    $this->token = $this->createMock('\\Drupal\\Core\\Utility\\Token');

    $userHandlerService = new InactiveUsersHandler(
      $this->configFactoryService,
      $this->logger,
      $this->languageManager,
      $this->timeService,
      $this->enEntityTypeManager,
      $this->state,
      $this->token,
      $this->mailManager,
    );
    $container->set('block_inactive_users.deactivate_users', $userHandlerService);
    \Drupal::setContainer($container);

  }

  /**
   * Utility function for getting a LoggerChannelFactory service.
   */
  private function getLoggerFactoryMock() {

    $loggerChannel = $this->createMock('Drupal\Core\Logger\LoggerChannel');
    $loggerChannel->expects($this->any())
      ->method('error')
      ->will($this->returnValue(''));

    $loggerChannelFactory = $this->createMock('Drupal\Core\Logger\LoggerChannelFactory');
    $loggerChannelFactory->expects($this->any())
      ->method('get')
      ->will($this->returnValue($loggerChannel));
    return $loggerChannelFactory;
  }

  /**
   * @covers  \Drupal\block_inactive_users\InactiveUsersHandler::disableInactiveUsersStatus
   *
   * Finds user idle users and disabled them
   */
  public function testDisableInactiveUsersStatus() {

    $user = $this->createMock('Drupal\user\Entity\User');

    $user->expects($this->any())
      ->method('isActive')
      ->will($this->returnValue(FALSE));

    $usersHandler = \Drupal::service('block_inactive_users.deactivate_users');
    $v = $usersHandler->disableInactiveUsersStatus($user, FALSE);
    $this->assertNotEmpty($v);
    $this->assertNotTrue($v->isActive());
  }

}
